const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'g7cqtm',
  e2e: {
    baseUrl: 'https://automationpratice.com.br/',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
