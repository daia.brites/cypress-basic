describe('Login', () => {

    it('Login com sucesso', () => {
        cy.visit('/')
            .get('#top_header')
        cy.get('.form-control')
            .type('teste@email.com')
        cy.get('.clear .theme-btn-one.btn_md')
            .click()
        cy.get('#swal2-title')
            .should('be.visible')
            .should('have.text', 'Success')

        
    })

    it('Login com sucesso clicando enter', () => {
        cy.visit('/')
            .get('#top_header')
        cy.get('.form-control')
            .type('teste@email.com{enter}')
        cy.get('#swal2-title')
            .should('be.visible')
            .should('have.text', 'Success')

        
    })

    it('Verificar fechamento da mensagem de sucesso', () => {
        cy.visit('/')
            .get('#top_header')

        cy.get('.form-control')
            .type('teste@email.com')

        cy.get('.clear .theme-btn-one.btn_md')
            .click()

        cy.get('.swal2-actions > .swal2-confirm')
            .click()

        cy.wait(1000)    
        cy.get('#swal2-title')
            .should('not.exist')        
    })

})